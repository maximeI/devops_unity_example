﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayToTheScreen : MonoBehaviour {

    [SerializeField]
    private InputField _input;

    [SerializeField]
    private Text _display;

    public void Display() {
        _display.text = _input.text;
    }
}
