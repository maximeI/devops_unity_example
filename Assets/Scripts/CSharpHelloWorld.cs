﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DLLTest;

public class CSharpHelloWorld : MonoBehaviour {
    [SerializeField]
    private Text _text;

    private void Start() {
        HelloWorld h = new HelloWorld();
        _text.text = h.GetMessage();
        
    }
}
    