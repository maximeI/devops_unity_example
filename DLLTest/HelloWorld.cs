﻿using System;

namespace DLLTest
{
    public class HelloWorld
    {
        protected string _message;

        public HelloWorld() {
            _message = "Hello World !";
        }

        public string GetMessage() {
            return _message;
        }
    }
}
