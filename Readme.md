This is an example of build Unity project with the version 2019.3.7f1 and 2017.4.36f1 (corresponding branch) using the Gitlab-CI tool.

You can get the build version in the repository -> CI /CD -> Pipelines -> Download artifacts of the pipeline that succeed.